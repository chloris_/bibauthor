mod author;
mod digestion;
mod ui;

use std::env;

/// Program entry point.
fn main() {
    /// Test string that is only used if the command line argument is "test"
    const TEST_STRING: &str = "Michael Hirscher a, ** , Volodymyr A. Yartys b, * , \
        Marcello Baricco c , Jose Bellosta von Colbe d , Didier Blanchard e , \
        Robert C. Bowman Jr. f , Darren P. Broom g , Craig E. Buckley h , \
        Fei Chang n , Ping Chen i , Young Whan Cho j , \
        Jean-Claude Crivello k , Fermin Cuevas k , William I.F. David l, m , \
        Petra E. de Jongh n , Roman V. Denys o , \
        Martin Dornheim d , Michael Felderhoff p , Yaroslav Filinchuk q , \
        George E. Froudakis r , David M. Grant s , Evan MacA. Gray z , \
        Bjørn C. Hauback b , Teng He t , Terry D. Humphries h , \
        Torben R. Jensen u , Sangryun Kim v , Yoshitsugu Kojima w , \
        Michel Latroche k , Hai-Wen Li x , Mykhaylo V. Lototskyy y , \
        Joshua W. Makepeace l , Kasper T. Møller h , Lubna Naheed z , \
        Peter Ngene n , Dag Noréus e , Mark Paskevicius h , Luca Pasquini ab , \
        Dorthe B. Ravnsbæk ac , M. Veronica Sofianos h , \
        Terrence J. Udovic ad , Tejs Vegge e , Gavin S. Walker s , \
        Colin J. Webb z , Claudia Weidenthaler p , Claudia Zlotea k";

    // Only continue if the user passed exatcly one command line argument
    if env::args().count() != 2 {
        eprintln!(
            "This program accepts exactly one argument \
            (list of authors)."
        );
        return;
    }
    let arg = env::args().nth(1).unwrap();

    // If the argument is "test", use `TEST_STRING` as input, otherwise use
    // the command line argument
    let input_string = match arg.as_str() {
        "test" => TEST_STRING,
        _ => &arg,
    };

    // Digest author list
    let authors = digestion::digest_author_list(input_string);
    // println!("{:#?}", authors);

    // Display the UI and let the user review and edit authors, and finally
    // print the final result
    let mut ui = ui::UI::new(authors);
    ui.display();
}
