//! Provides interactive command line interface for inspecting digested
//! list of names, correcting and exporting names.

use crate::author::Author;
use std::io;

/// Default separator put between names of two authors
const DEFAULT_AUTHOR_SEPARATOR: &str = " and ";

/// Horizontal line used in the UI
const HORIZONTAL_LINE: &str =
    "----------------------------------------------------------------------";

/// Horizontal space used to separate multiple command options
const HORIZONTAL_SPACE: &str = "    ";

/// Implements interactive command line interface for inspecting, editing and
/// exporting digested list of author names.
pub struct UI {
    /// Digested author data
    data: Vec<Author>,
    /// Separator put between names of two authors
    pub author_separator: String,
}

impl UI {
    /// Returns a new UI object initialied digested author data (`data`).
    pub fn new(data: Vec<Author>) -> Self {
        Self {
            data: data,
            author_separator: String::from(DEFAULT_AUTHOR_SEPARATOR),
        }
    }

    /// Displays the UI and waits until user interaction is over.
    pub fn display(&mut self) {
        enum UIAction {
            DisplayAuthorList,
            EditAuthor(usize),
            EditAuthorSeparator,
            PrintAuthorList,
        }

        if self.data.is_empty() {
            eprintln!("Empty author list. There is nothing to display.");
            return;
        }

        let mut action = UIAction::DisplayAuthorList;
        // Main UI loop
        loop {
            match action {
                UIAction::DisplayAuthorList => {
                    self.display_author_table();
                    self.display_prompt_author_table();
                }
                UIAction::EditAuthor(i) => {
                    // Convert user-friendly one-based index to real
                    // zero-based index
                    let i = i - 1;
                    let mut edit_dialog = AuthorEditDialog::new(self.data[i].clone());
                    match edit_dialog.display() {
                        Some(author) => {
                            // Replace the author with new data
                            self.data[i] = author;
                        }
                        None => {
                            // Delete the author
                            self.data.remove(i);
                        }
                    }
                    action = UIAction::DisplayAuthorList;
                    continue;
                }
                UIAction::EditAuthorSeparator => {
                    if let Some(separator) = UI::get_author_separator() {
                        self.author_separator = separator;
                    }
                    action = UIAction::DisplayAuthorList;
                    continue;
                }
                UIAction::PrintAuthorList => {
                    println!("{}", HORIZONTAL_LINE);
                    println!("{}", self.export_author_list());
                    break;
                }
            }

            // Input loop
            loop {
                let mut input = String::new();
                match io::stdin().read_line(&mut input) {
                    Ok(_) => {
                        // Parse input
                        let trim = input.trim();
                        if trim.chars().all(|c| c.is_ascii_digit()) {
                            // Digits only mean author correction
                            match trim.parse::<usize>() {
                                Ok(i) => {
                                    if i > 0 && i <= self.data.len() {
                                        action = UIAction::EditAuthor(i);
                                        break;
                                    } else {
                                        println!("Author number {} is out of bounds.", i);
                                    }
                                }
                                Err(error) => {
                                    println!("Could not parse author number: '{}'.", error);
                                }
                            }
                        } else {
                            // Otherwise it can be one of the few commands
                            match trim {
                                "p" => {
                                    action = UIAction::PrintAuthorList;
                                    break;
                                }
                                "s" => {
                                    action = UIAction::EditAuthorSeparator;
                                    break;
                                }
                                _ => {
                                    println!("Input '{}' not recognized.", trim);
                                }
                            }
                        }
                    }
                    Err(error) => {
                        eprintln!("Error while reading from stdin: {}", error);
                        break;
                    }
                }
            }
        }
    }

    /// Prints table with author numbers, formatted author names, and
    /// source author strings to the standard output.
    fn display_author_table(&self) {
        println!("\n{}", HORIZONTAL_LINE);
        for (i, author) in self.data.iter().enumerate() {
            println!(
                "({:2}) | {:30} | {:30}",
                i + 1,
                UI::author_name(author),
                author.source,
            );
        }
    }

    /// Displays a prompt that should be placed under the author table.
    fn display_prompt_author_table(&self) {
        println!("{}", HORIZONTAL_LINE);
        print!("1-{} - edit author{}", self.data.len(), HORIZONTAL_SPACE);
        print!("s - edit separator{}", HORIZONTAL_SPACE);
        println!("p - print author list");
    }

    /// Formats author name as "surname1 surname2, name1 name2" by joining
    /// names with space.
    ///
    /// Joins only names or only surnames if only single field type is available.
    /// If no fields are available, an empty `String` is returned.
    fn author_name(author: &Author) -> String {
        if author.surnames.is_empty() && author.names.is_empty() {
            String::new()
        } else if author.names.is_empty() {
            author.surnames.join(" ")
        } else if author.surnames.is_empty() {
            author.names.join(" ")
        } else {
            format!("{}, {}", author.surnames.join(" "), author.names.join(" "))
        }
    }

    /// Exports the authors collection as list of authors separated by
    /// `self.author_separator`. Author names are formatted by
    /// `author_name()`.
    fn export_author_list(&self) -> String {
        let mut authors = Vec::<String>::new();
        for author in self.data.iter() {
            authors.push(UI::author_name(&author));
        }
        authors.join(&self.author_separator)
    }

    /// Prompts the user for author separator, reads it from stdin and
    /// returns it. `None` is returned in case of IO error.
    fn get_author_separator() -> Option<String> {
        clear_screen();
        println!("Enter new author separator:");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_) => match input.strip_suffix('\n') {
                Some(stripped) => Some(String::from(stripped)),
                None => Some(input),
            },
            Err(error) => {
                eprintln!("Error while reading new author separator: {}", error);
                None
            }
        }
    }
}

/// Provides interactive dialog for editing author fields.
struct AuthorEditDialog {
    /// Author data to be edited
    author: Option<Author>,
}

impl AuthorEditDialog {
    /// Creates a new dialog for editing field of author `author`.
    fn new(author: Author) -> Self {
        Self {
            author: Some(author),
        }
    }

    /// Displays the dialog that allows the user to edit author fields.
    /// Either changed author data or `None` is returned. `None` means that
    /// the author should be deleted.
    fn display(&mut self) -> Option<Author> {
        enum DialogAction {
            DisplayFields,
            EditField(usize),
            DeleteAuthor,
            Back,
        }

        let mut action = DialogAction::DisplayFields;

        // Dialog loop
        loop {
            match action {
                DialogAction::DisplayFields => {
                    self.print_fields_and_prompt();
                }
                DialogAction::EditField(j) => {
                    self.edit_field(j);
                    action = DialogAction::DisplayFields;
                    continue;
                }
                DialogAction::DeleteAuthor => {
                    self.author.take();
                    action = DialogAction::Back;
                    continue;
                }
                DialogAction::Back => {
                    return self.author.take();
                }
            }

            // Input loop
            loop {
                let mut input = String::new();
                match io::stdin().read_line(&mut input) {
                    Ok(_) => {
                        // Parse input
                        let trim = input.trim();
                        if trim.chars().all(|c| c.is_ascii_digit()) {
                            // Digits mean field correction
                            match trim.parse::<usize>() {
                                Ok(j) => {
                                    if j > 0 && j <= self.number_of_fields() {
                                        action = DialogAction::EditField(j);
                                        break;
                                    } else {
                                        println!("Field number {} is out of bounds.", j);
                                    }
                                }
                                Err(error) => {
                                    println!("Could not parse field number: '{}'.", error);
                                }
                            }
                        } else {
                            match trim {
                                "d" => {
                                    action = DialogAction::DeleteAuthor;
                                    break;
                                }
                                "b" => {
                                    action = DialogAction::Back;
                                    break;
                                }
                                _ => {
                                    println!("Input '{}' not recognized.", trim);
                                }
                            }
                        }
                    }
                    Err(error) => {
                        eprintln!("Error while reading from stdin: {}", error);
                        return self.author.take();
                    }
                }
            }
        }
    }

    /// Prints author fields and commands to edit them. Assumes that field
    /// `self.author` is not `None`.
    fn print_fields_and_prompt(&self) {
        let author = self.author.as_ref().unwrap();

        clear_screen();
        println!("Author names:");
        for (i, name) in author.names.iter().enumerate() {
            println!("({}) {}", i + 1, name);
        }
        println!("\nAuthor surnames:");
        for (i, name) in author.surnames.iter().enumerate() {
            println!("({}) {}", author.names.len() + i + 1, name);
        }
        println!("\nSource text:");
        println!("{}", author.source);
        println!("{}", HORIZONTAL_LINE);
        print!(
            "(1-{}) edit field{}",
            author.names.len() + author.surnames.len(),
            HORIZONTAL_SPACE
        );
        print!("d - delete author{}", HORIZONTAL_SPACE);
        println!("b - back");
    }

    /// Prompts for new value of author field with index `j` (begins with 1)
    /// and includes both names and surnames, respectively. The field is
    /// then changed or deleted if empty value is supplied. Assumes that
    /// `self.author` is not `None`.
    fn edit_field(&mut self, j: usize) {
        /// Used in translation of author field index, that begins with 1
        /// and includes both names and surnames, to field type and real
        /// index in the vector.
        enum AuthorField {
            Name(usize),
            Surname(usize),
        }

        /// Parses field index `field_index` to type of the field and
        /// real index in the vector, in regards to the author record
        /// `author`. Panics if the index is out of bounds.
        /// Assumes that `self.author` is not `None`.
        impl AuthorField {
            fn parse(author: &Author, field_index: usize) -> AuthorField {
                let n_names = author.names.len();
                let n_surnames = author.surnames.len();

                if field_index < 1 || field_index > (n_names + n_surnames) {
                    panic!("Field index {} is out of bounds.", field_index);
                } else if field_index <= n_names {
                    return AuthorField::Name(field_index - 1);
                } else {
                    return AuthorField::Surname(field_index - n_names - 1);
                }
            }
        }

        let author = self.author.as_mut().unwrap();

        // Read new field name from stdin
        println!("{}", HORIZONTAL_LINE);
        println!(
            "Enter new value for field {} (empty value deletes field):",
            j
        );
        let mut input = String::new();
        if let Err(error) = io::stdin().read_line(&mut input) {
            eprintln!("Error while reading new field name from stdin: {}", error);
            return;
        }
        let input = input.trim();

        // Modify or delete the appropriate author field
        let field = AuthorField::parse(author, j);
        match field {
            AuthorField::Name(i) => {
                if input.is_empty() {
                    author.names.remove(i);
                } else {
                    author.names[i] = String::from(input);
                }
            }
            AuthorField::Surname(i) => {
                if input.is_empty() {
                    author.surnames.remove(i);
                } else {
                    author.surnames[i] = String::from(input);
                }
            }
        }
    }

    /// Returns the number of fields (sum of names and surnames) in the
    /// author record. Assumes that `self.author` is not `None`.
    fn number_of_fields(&self) -> usize {
        let author = self.author.as_ref().unwrap();
        return author.names.len() + author.surnames.len();
    }
}

/// Clears the screen and positions the cursor in the top left corner.
fn clear_screen() {
    print!("\x1B[2J\x1B[1;1H");
}
