//! Contains functions to digest input author list in form
//! "name surname, name surname ...".

use crate::author::Author;

/// Characters, other than simple letters, that often appear after author's
/// name and surname to indicate a note with their affiliation or email.
const FOOTNOTE_CHARACTERS: [char; 1] = ['*'];
/// Character sequences, frequently appearing in surnames, that should not
/// be treated as footnotes, even though they are short sequences of
/// lowercase letters.
const NOT_FOOTNOTE_WORDS: [&str; 3] = ["de", "van", "der"];

/// Digests input author list (`input_string`) into a list of authors with
/// known names and surnames, that can be freely formatted later.
pub fn digest_author_list(input_string: &str) -> Vec<Author> {
    let mut authors: Vec<Author> = Vec::new();

    // Replace potential "and" with ","
    let input_string = input_string.replace("and", ",");

    // Split the input by authors
    for author_string in input_string.split(',') {
        match process_author(author_string) {
            Ok(author) => authors.push(author),
            Err(e) => {
                eprintln!("Could not parse '{}': {}", author_string, e)
            }
        }
    }
    return authors;
}

/// Extracts specific author information from whole author string
/// (format "name surname", possibly including footnote characters
/// at the end or additional names with initials) and returns it as
/// an `Author` struct.
fn process_author(author_string: &str) -> Result<Author, &str> {
    #[derive(Debug)]
    enum Stages {
        Names,
        Initials,
        Surnames,
        Footnotes,
    }

    let mut author = Author::new();
    author.source = String::from(author_string);
    let mut stage = Stages::Names;

    for word in author_string.trim().split(" ") {
        #[cfg(test)]
        println!("Word '{}', stage '{:?}'", word, stage);

        match stage {
            Stages::Names => {
                if is_footnote(word) {
                    stage = Stages::Footnotes;
                } else if is_initial(word) {
                    stage = Stages::Initials;
                    let word = remove_trailing_footnotes(word);
                    author.names.push(word);
                } else {
                    let word = remove_trailing_footnotes(word);

                    // If surnames begin without initials in-between
                    if author.names.len() > 0 {
                        stage = Stages::Surnames;
                        author.surnames.push(word);
                    } else {
                        author.names.push(word);
                    }
                }
            }
            Stages::Initials => {
                if is_initial(word) {
                    author.names.push(String::from(word));
                } else {
                    stage = Stages::Surnames;
                    let word = remove_trailing_footnotes(word);
                    author.surnames.push(word);
                }
            }
            Stages::Surnames => {
                if is_initial(word) {
                    return Err("Encountered an initial among surnames");
                } else if is_footnote(word) {
                    stage = Stages::Footnotes;
                } else {
                    let word = remove_trailing_footnotes(word);
                    author.surnames.push(word);
                }
            }
            Stages::Footnotes => {
                if !is_footnote(word) {
                    return Err("Encountered something else than a footnote among footnotes");
                }
            }
        }
    }

    #[cfg(test)]
    println!("Finished, stage '{:?}'\n", stage);

    if author.names.is_empty() && author.surnames.is_empty() {
        if let Stages::Footnotes = stage {
            return Err("The string consisted only of footnotes");
        }
        return Err("Author has no names or surnames");
    }
    if author.names.is_empty() {
        return Err("Author has no names");
    }
    if author.surnames.is_empty() {
        return Err("Author has no surnames");
    }

    return Ok(author);
}

/// Check is given string represents an initial and returns the result.
///
/// String is treated as an initial if it contains uninterrupted groups of
/// single uppercase letter and single dot. The last dot is considered
/// optional.
fn is_initial(string: &str) -> bool {
    enum Stages {
        Start,
        FoundInitial,
        FoundDot,
    }

    let mut stage = Stages::Start;
    for c in string.chars() {
        match stage {
            Stages::Start | Stages::FoundDot => {
                if c.is_uppercase() {
                    stage = Stages::FoundInitial;
                } else {
                    return false;
                }
            }
            Stages::FoundInitial => {
                if c == '.' {
                    stage = Stages::FoundDot;
                } else {
                    return false;
                }
            }
        }
    }

    // The string was empty
    if let Stages::Start = stage {
        return false;
    }

    // The string conforms to the rules and is not empty
    return true;
}

/// Determines if given string is a footnote mark.
///
/// The string is treated as a footnote if it is non-empty and is either a
/// sequence of at most two characters from `FOOTNOTE_CHARACTERS`, or a
/// sequence of at most two lowercase characters. Multiple footnotes,
/// separated with a comma, are supported. The comma can be the last
/// character in the string.
fn is_footnote(string: &str) -> bool {
    #[derive(Debug)]
    enum Stages {
        Start,
        LowercaseCharacter,
        FootnoteCharacter,
        Comma,
        NextFootnote,
    }

    if NOT_FOOTNOTE_WORDS.contains(&string) {
        return false;
    }

    let mut stage = Stages::Start;
    for c in string.chars() {
        #[cfg(test)]
        println!("Character '{}', stage '{:?}'", c, stage);

        match stage {
            Stages::Start | Stages::NextFootnote => {
                if FOOTNOTE_CHARACTERS.contains(&c) {
                    stage = Stages::FootnoteCharacter;
                } else if c.is_lowercase() {
                    stage = Stages::LowercaseCharacter;
                } else {
                    return false;
                }
            }
            Stages::LowercaseCharacter => {
                if c.is_lowercase() {
                    stage = Stages::Comma;
                } else if c == ',' {
                    stage = Stages::NextFootnote;
                } else {
                    return false;
                }
            }
            Stages::FootnoteCharacter => {
                if FOOTNOTE_CHARACTERS.contains(&c) {
                    stage = Stages::Comma;
                } else if c == ',' {
                    stage = Stages::NextFootnote;
                } else {
                    return false;
                }
            }
            Stages::Comma => {
                if c == ',' {
                    stage = Stages::NextFootnote;
                } else {
                    return false;
                }
            }
        }
    }

    #[cfg(test)]
    println!("Finished, stage '{:?}'", stage);

    // String is empty
    if let Stages::Start = stage {
        return false;
    }

    // String conforms to all rules to be treated as a footnote
    return true;
}

/// Removes trailing footnote characters from author name `string` and
/// returns the name without them. Only eliminates characters at the end
/// of the string.
fn remove_trailing_footnotes(string: &str) -> String {
    let mut string = String::from(string);
    while let Some(c) = string.pop() {
        if !FOOTNOTE_CHARACTERS.contains(&c) {
            string.push(c);
            break;
        }
    }

    return string;
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Tests if `is_initial` detects initials correctly.
    #[test]
    fn test_is_initial() {
        const INITIALS: [&str; 4] = ["A", "X.", "W", "I.F."];
        const NOT_INITIALS: [&str; 5] = ["van", "Orimo", "d", "and", "Xy."];

        for initial in &INITIALS {
            if !is_initial(initial) {
                panic!("'{}' should be recognized as an initial", initial);
            }
        }

        for not_initial in &NOT_INITIALS {
            if is_initial(not_initial) {
                panic!("'{}' should not be recognized as an initial", not_initial);
            }
        }
    }

    /// Tests if `is_footnote` detects footnotes correctly.
    #[test]
    fn test_is_footnote() {
        const FOOTNOTES: [&str; 4] = ["a", "b,*", "a,**", "ad"];
        const NOT_FOOTNOTES: [&str; 4] = ["van", "J.", "D", "-"];

        for fnote in &FOOTNOTES {
            if !is_footnote(fnote) {
                panic!("'{}' should be recognized as a footnote", fnote);
            }
        }

        for not_fnote in &NOT_FOOTNOTES {
            if is_footnote(not_fnote) {
                panic!("'{}' should not be recognized as a footnote", not_fnote);
            }
        }
    }

    /// Tests if `process_author` precesses author string correctly.
    #[test]
    fn test_process_author() {
        // Tuple of input text and expected results: (&str, Author)
        let inputs_results = vec![
            (
                "M. Veronica Sofianos h",
                Author {
                    names: vec![String::from("M.")],
                    surnames: vec![String::from("Veronica"), String::from("Sofianos")],
                    source: String::new(),
                },
            ),
            (
                "Kasper T. Møller h",
                Author {
                    names: vec![String::from("Kasper"), String::from("T.")],
                    surnames: vec![String::from("Møller")],
                    source: String::new(),
                },
            ),
            (
                "William I.F. David l, m",
                Author {
                    names: vec![String::from("William"), String::from("I.F.")],
                    surnames: vec![String::from("David")],
                    source: String::new(),
                },
            ),
            (
                "Jean-Claude Crivello k",
                Author {
                    names: vec![String::from("Jean-Claude")],
                    surnames: vec![String::from("Crivello")],
                    source: String::new(),
                },
            ),
            (
                "Jose Bellosta von Colbe d",
                Author {
                    names: vec![String::from("Jose")],
                    surnames: vec![
                        String::from("Bellosta"),
                        String::from("von"),
                        String::from("Colbe"),
                    ],
                    source: String::new(),
                },
            ),
            (
                "Petra de Jongh",
                Author {
                    names: vec![String::from("Petra")],
                    surnames: vec![String::from("de"), String::from("Jongh")],
                    source: String::new(),
                },
            ),
            (
                "Johannes D. van der Waals",
                Author {
                    names: vec![String::from("Johannes"), String::from("D.")],
                    surnames: vec![
                        String::from("van"),
                        String::from("der"),
                        String::from("Waals"),
                    ],
                    source: String::new(),
                },
            ),
            (
                "Baoming Ji*",
                Author {
                    names: vec![String::from("Baoming")],
                    surnames: vec![String::from("Ji")],
                    source: String::new(),
                },
            ),
            (
                "Jerry L. Atwood*",
                Author {
                    names: vec![String::from("Jerry"), String::from("L.")],
                    surnames: vec![String::from("Atwood")],
                    source: String::new(),
                },
            ),
        ];

        // Test code
        for (input, desired_result) in inputs_results {
            let result = process_author(input);
            let result = result.unwrap();
            assert_eq!(result.names, desired_result.names);
            assert_eq!(result.surnames, desired_result.surnames);
        }
    }

    /// Tests if `remove_trailing_footnotes` removes footnotes correctly.
    #[test]
    fn test_remove_trailing_footnotes() {
        // And array of tuples (input, expected result).
        let test_data = [
            ("Li*", "Li"),
            ("Hammarström**", "Hammarström"),
            ("Silly*test***", "Silly*test"),
        ];

        // Test code
        for (input, expected_result) in test_data.iter() {
            let result = remove_trailing_footnotes(input);
            assert_eq!(result.as_str(), *expected_result);
        }
    }
}
