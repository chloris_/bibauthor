//! Provides structs to represent author information.

/// Contains information about a single author.
#[derive(Debug)]
#[derive(Clone)]
pub struct Author {
    /// Names of the author
    pub names: Vec<String>,
    /// Surnames of the author
    pub surnames: Vec<String>,
    /// Source string from which names and surnames were derived
    pub source: String,
}

impl Author {
    /// Returns a new instance of Author with empty fields.
    pub fn new() -> Self {
        Self {
            names: Vec::new(),
            surnames: Vec::new(),
            source: String::new(),
        }
    }
}
