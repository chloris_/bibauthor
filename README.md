# bibauthor

An interactive CLI author names formatter for LaTeX bibliography written in Rust.


## Building

Obtain the source and simply run the command below:
``` bash
cargo build --release
```
The binary should appear in subdirectory `target/release`.


## Usage

Run the program with a single parameter only: list of authors. For example:
```bash
bibauthor "Robin Tyburski, Tianfei Liu, Starla D. Glover*, and Leif Hammarström*"
```

The program will display an interactive interface, which allows editing
author fields and deleting them, changing desired author separator
(`" and "`) by default, and printing the final result. By default, it
should look like this:
```text
Tyburski, Robin and Liu, Tianfei and Glover, Starla D. and Hammarström, Leif
```

The program can instead operate on some test values if ran with parameter `test`:
```bash
bibauthor test
```
